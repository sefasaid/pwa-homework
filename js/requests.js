app1.controller("Requests", [ "$http", function($http) {
    var ctrl = this;
    ctrl.last5 = true;
    ctrl.filter = '';
    ctrl.accounts = [];

    ctrl.refreshHistory = function() {
        var to = ctrl.last5 ? 5 : 999999;
        $http.get('/unverified?from=1&to=' + to + '&filter=' + ctrl.filter).then(
            function(rep) { ctrl.accounts = rep.data; },
            function(err) { ctrl.message = 'Error retrieving history'; }
        );
    };
    
    ctrl.approve = function(id){
        $http.post('/unverified',{id :id}).then(
            function(rep) { 
                ctrl.refreshHistory();
             },
            function(err) { ctrl.message = 'Error retrieving history'; }
        );
    }
    ctrl.remove = function(id){
        $http.delete('/unverified?id='+id).then(
            function(rep) { 
                ctrl.refreshHistory();
             },
            function(err) { ctrl.message = 'Error retrieving history'; }
        );
    }
    ctrl.refreshHistory();
}]);
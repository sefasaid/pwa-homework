app1.controller("Home", ['$http', 'common', function ($http, common) {
    var ctrl = this;
    ctrl.session = common.session;
    ctrl.message = {};

    ctrl.creds = { email: 'aaa@aaa.com', password: 'a' };
    ctrl.reg = { email: 'aaa@aaa.com', password: 'a' };

    ctrl.doLogin = function () {
        $http.post('/login', ctrl.creds).then(
            function (rep) { ctrl.session.login = rep.data.email; common.rebuildMenu(); ctrl.message = { ok: 'Login successful' }; },
            function (err) { ctrl.message = { error: 'Login failed' }; }
        );
    }

    ctrl.doLogout = function () {
        $http.delete('/login').then(
            function (rep) { ctrl.session.login = null; common.rebuildMenu(); ctrl.message = { ok: 'Logout successful' }; }
        );
    }
    ctrl.doRegister = function () {
        $http.post('/register', ctrl.reg).then(
            function (rep) { ctrl.session.login = rep.data.email; common.rebuildMenu(); ctrl.message = { ok: 'Login successful' }; },
            function (err) { ctrl.message = { error: 'Login failed' }; }
        );
    }
}]);